#Projet BomberWoman 2019
========================

* DOMINGUES Kévin p1607884
* LIKA Dreni p1710717
* MONTANT Loann p1709796

Présentation
------------
	Les étudiants DOMINGUES Kévin (11607884), LIKA Dreni (11710717) et MONTANT Loann (11709796), propose ainsi la réalisation
	d’un logiciel de type jeu vidéo exécutable sur une plateforme PC en C++/SDL2.
	Ce jeu se présente sous forme la forme d’un plateau de jeu 2D, sur lequel plusieurs joueurs devront s’affronter.
	L’objectif pour chaque joueur sera de faire exploser dans un premier temps les murs pour obtenir des bonus, et ainsi
	se préparer au mieux a la rencontre des joueurs adverse, le but étant de faire exploser ses adversaires afin
	d’être le dernier survivant.
			
Description de la demande et contrainte
---------------------------------------
	Les étudiants en LIFAP4 de l’université lyon 1, ont pour mission de réaliser un projet sur un sujet libre ( Application ou Jeu ).
	Le projet doit être réalisé par groupe de 3.
	Ils seront évalué sur 3 critères, à savoir une note “Technique”, “Conception”, et “Organisation”.
	La seule contrainte est la date de rendu du projet qui est du 6 mai 2019.
	Code écrit en C++.
	Se compile avec g++.

Travail effectué
----------------

* DOMINGUES Kévin	Toute la partie codage du projet et son execution.
* MONTANT Loann		Le visuel et design.
* LIKA Dreni		...

